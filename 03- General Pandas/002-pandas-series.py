import numpy as np
import pandas as pd

my_list = [10, 20, 30]
arr = np.array([10, 20, 30])
labels = ['a', 'b', 'c']
d = { 'a': 10, 'b': 20, 'c': 100 }

pd.Series(my_list)
# 0    10
# 1    20
# 2    30
# dtype: int64
pd.Series(arr)
# 0    10
# 1    20
# 2    30
# dtype: int32

pd.Series(my_list, index=labels)
# a    10
# b    20
# c    30
# dtype: int64
pd.Series(d)
# a     10
# b     20
# c    100
# dtype: int64

series1 = pd.Series([1, 2, 3, 4], ['USA', 'BEL', 'NLD', 'FRA'])
# USA    1
# BEL    2
# NLD    3
# FRA    4
# dtype: int64
series2 = pd.Series([1, 2, 3, 4], ['USA', 'BEL', 'ITA', 'JPN'])
# USA    1
# BEL    2
# ITA    3
# JPN    4
# dtype: int64

print(
    series1 + series2
)
