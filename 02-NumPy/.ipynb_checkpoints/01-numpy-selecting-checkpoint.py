import numpy as np

arr = np.arange(0, 11)
copy_arr = arr.copy()
# print(arr)         #   [ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10]

arr[0:5] = 100
# print(arr)         #   [100, 100, 100, 100, 100,   5,   6,   7,   8,   9,  10]

arr[:] = 99
# print(arr)         #   [99, 99, 99, 99, 99, 99, 99, 99, 99, 99, 99]
# print(copy_arr)    #   [ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10]

matrix = np.array([[5, 10, 15], [3, 6, 9], [1, 2, 3]])
# [ [ 5, 10, 15],  [ 3,  6,  9],  [ 1,  2,  3] ]
# print(matrix[0][0])     #     5
# print(matrix[0])        #     [ 5 10 15]
# print(matrix[:, 2])     #     [15  9  3]
# print(matrix[:2, 1:])   #     [ [10 15], [ 6  9] ]
# print(matrix[1:, :2])   #     [ [3 6], [1 2] ]

arr = np.arange(1, 11)
# [ 1  2  3  4  5  6  7  8  9 10]
bool_arr = arr > 4
# [False False False False  True  True  True  True  True  True]
# print(arr[bool_arr])        #       [ 5  6  7  8  9 10]
# print(arr[arr > 4])         #       [ 5  6  7  8  9 10]

